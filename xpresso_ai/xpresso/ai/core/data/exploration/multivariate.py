from xpresso.ai.core.data.exploration import Explorer
from xpresso.ai.core.data.exploration import Understander
from sklearn.base import BaseEstimator

class MultivariateExplorer(BaseEstimator):
    """
    Performs Mutivariate exploration on the dataset object
    """
    def fit(self,dataset):
        """
        Performs multivariate analytis on the dataset
        Args:
            dataset (StructuredDataset): StructuredDataset object
        """
        if not len(dataset.info.attributeInfo):
            Understander().fit(dataset)

        explorer = Explorer(dataset)
        explorer.explore_multivariate()