MOUNT_PATH = None
import os
import glob
import types
import pickle
import shutil
import marshal
import numpy as np
import tensorflow as tf
from keras import layers
from tensorflow import keras
import matplotlib.pyplot as plt
from keras.preprocessing.image import ImageDataGenerator
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
## $xpr_param_component_name = data_train
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = []

model = keras.models.Sequential([
        tf.keras.layers.Conv2D(16, (3,3), padding='same', activation='relu', input_shape=(150,150,3)),
        tf.keras.layers.MaxPooling2D(2,2),

        tf.keras.layers.Conv2D(32, (3,3), padding='same', activation='relu'),
        tf.keras.layers.MaxPooling2D(2,2),

        tf.keras.layers.Conv2D(64, (3,3), padding='same', activation='relu'),
        tf.keras.layers.MaxPooling2D(2,2),

        tf.keras.layers.Flatten(),
        tf.keras.layers.Dropout(.2),
        tf.keras.layers.Dense(512, activation='relu'),

        tf.keras.layers.Dropout(.2),
        tf.keras.layers.Dense(5, activation='softmax')

])

# Compile the model
model.compile(optimizer='adam', loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
epochs = 80

history = model.fit_generator(
    train_data_gen, 
    steps_per_epoch=int(np.ceil(train_data_gen.n / float(batch_size))),
    epochs=epochs,
    validation_data=val_data_gen,
    validation_steps=int(np.ceil(val_data_gen.n / float(batch_size))))
